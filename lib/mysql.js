var util = require("util"),
    winston = require("winston"),
    uuid = require('node-uuid');

var Mysql = winston.transports.Mysql = function(config) {

  if (! config.table) {
    throw new Error("Please provide a destination MySQL table.");
  }

  if (! config.mysql) {
    throw new Error("Please provide node-mysql configuration object.");
  }

  // name of this Transport
  this.name = "Mysql";

  // store logs for this level and above
  this.level = config.level || "info";

  // table name in mysql
  this.table = config.table || "winston_logs";
  
  // node-mysql configuration details 
  this.mysql = config.mysql || {
    "host": "",
    "user": "",
    "password": "",
    "database": ""
  };

  this.pool = config.pool || {
    "min": 2,
    "max": 10
  };

  // store extra metadata (e.g host, production/staging)
  // for each entry into the log
  this.environment = config.environment || {};
};

util.inherits(Mysql, winston.Transport);

Mysql.prototype.log = function(level, message, metadata, cb) {

  var entry = {
    uuid: uuid.v4(),
    level: level,
    message: message,
    metadata: JSON.stringify(metadata),
    environment: JSON.stringify(this.environment),
    timestamp: new Date().toISOString().replace(/T/, " ").replace(/\..+/, "")
  };

  var knex = require("knex")({
    client: "mysql",
    connection: this.mysql,
    pool: this.pool
  });

  knex(this.table)
    .insert(entry)
    .then(function() {
      return cb();
    });
};

exports.Mysql = Mysql;