# my-winston

MySQL Transport for Winston. Based on the original code by github.com/mneme/winston-mysqlM, this aims to be a bit more extensible with expanded documentation.


## Getting started

1) Set up a table as below:

```sql

CREATE TABLE `winston_logs` (
  `uuid` varchar(36) NOT NULL DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `message` text,
  `metadata` text,
  `timestamp` timestamp NULL DEFAULT NULL,
  `environment` text,
  PRIMARY KEY (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

```

2) Include in your application as below. Requiring the transport into your app creates ```log.transports.Mysql``` which can be configured similar to any other transport.

```javascript

var log = require("winston"),
     myWinston = require("my-winston"),
     os = require("os");

log.add(log.transports.Mysql, {

  // node-mysql connection configuration
  mysql: {},

  // destination MySQL table 
  table: "winston_logs", 

  // minimum level of logs to store
  level: "info", 

  // include relevant environment details in this object
  environment: {
    host: os.hostname()
  }
});

```

3) Collect logs and query!

```sql
SELECT
	*
FROM
	winston_logs
WHERE
	level = "info"
	AND json_extract(environment,'host') = "production"
;
```
([MySQL UDFs from MySQL labs](http://labs.mysql.com/) provide useful json functions - not recommended for a production instance but still useful for log analysis on unstructured json data)


## Todo before release

* Unit tests
* Extensible/flexible logging structure